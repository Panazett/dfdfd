﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour {

	public GameObject[] obj;
	public float spawnMin = 0.9f;
	public float spawnMax = 0.9f;

	public int types;
	public bool started;

	// Use this for initialization
	void Start () {
		types = 1;
		Spawn ();
	}

	void Update () {
		if (PlayerControllerScript.starting && !started) {
			spawnMin = 0.7f;
			spawnMax = 0.9f;
			started = true;
			types = 2;
		}
	}
	
	// Update is called once per frame
	void Spawn () {
		Instantiate (obj [Random.Range (0, types)], transform.position, Quaternion.identity);
		Invoke ("Spawn", Random.Range (spawnMin, spawnMax));
	}
}
