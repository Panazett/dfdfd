﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerScript : MonoBehaviour {

	public GameObject[] obj;
	public float spawnMin;
	public float spawnMax;

	public float groundRadius = 0.4f;
	public bool groundedLeft = false;
	public bool groundedRight = false;
	public Transform groundCheckLeft;
	public Transform groundCheckRight;
	public LayerMask whatIsGround;

	public bool started;

	void Start () {
		spawnMin = 0.6f;
		spawnMax = 2f;
	}

	void Update () {
		if (PlayerControllerScript.starting && !started) {
			Invoke ("Spawn", 1);
			started = true;
		}
	}

	void Spawn () {
		if (groundedLeft && groundedRight) {
			Instantiate (obj [Random.Range (0, obj.Length)], transform.position, Quaternion.identity);
			Invoke ("Spawn", Random.Range (spawnMin, spawnMax));
		} else
			Invoke ("Spawn", Random.Range (spawnMin, spawnMax));
	}

	void FixedUpdate () {
		groundedLeft = Physics2D.OverlapCircle (groundCheckLeft.position, groundRadius, whatIsGround);
		groundedRight = Physics2D.OverlapCircle (groundCheckRight.position, groundRadius, whatIsGround);
	}

}
