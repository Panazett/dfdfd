﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControllerScript : MonoBehaviour {

	public float movementSpeed = 10f;
	public float jumpForce = 1600f;

	public bool attacking = false;
	public float attackForce = 4000;

	public float groundRadius = 0.2f;
	public bool grounded = false;
	public Transform groundCheck;
	public LayerMask whatIsGround;

	public float duration;
	public static bool starting = false;

	public Rigidbody2D rb;

	public Text scoreText;

	public AudioClip attack;
	AudioSource soundAttack;

	public GameObject obj;

	void Start () {
		starting = false;
		rb = GetComponent<Rigidbody2D> ();
		soundAttack = GetComponent<AudioSource>();
		//obj = GetComponent<GameObject> ();
	}

	void Update () {
		if (grounded && Input.GetKeyDown (KeyCode.Space)) {
			rb.velocity = new Vector2 (rb.velocity.x, 0);
			rb.AddForce (new Vector2 (0, jumpForce));
			attacking = false;
		}
		if (!grounded && !attacking && Input.GetKeyDown (KeyCode.Space)) {
			attacking = true;
			rb.velocity = new Vector2 (rb.velocity.x, 0);
			rb.AddForce (new Vector2 (0, -attackForce));
			if (!starting)
				starting = true;

		}

		duration += Time.deltaTime*10;
		scoreText.text = Mathf.Round(duration).ToString ();
	}

	void FixedUpdate () {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		if (grounded && attacking) {
			rb.velocity = new Vector2 (rb.velocity.x, 0);
			rb.AddForce (new Vector2 (0, jumpForce * 1.45f));
			attacking = false;
		}
			
		rb.velocity = new Vector2 (1 * movementSpeed, rb.velocity.y);

	}

	void OnTriggerEnter2D (Collider2D enemy) {
		if (attacking && enemy.tag == "Enemy") {
			Instantiate (obj, enemy.transform.position, Quaternion.identity);
			Destroy (enemy.gameObject);
			rb.velocity = new Vector2 (rb.velocity.x, 0);
			rb.AddForce (new Vector2 (0, jumpForce * 1.45f));
			attacking = false;
			soundAttack.PlayOneShot(attack,1);
		} else if (enemy.tag == "Enemy") {
			
		}
	}


}
